<?php
/**
 * Created by PhpStorm.
 * User: DiemND
 * Date: 10/14/20
 * Time: 11:48 AM
 */

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class IndexController extends Controller
{
    public function getBugSummary (Request $request) {
        $filterTime = $request->input('filterTime');
        $issues = [];
        if ($filterTime) {
            $arrFilterTime = json_decode($filterTime);
            foreach ($arrFilterTime as $itemTime) {
                $fromDate = $itemTime->fromTime;
                $fromTime = $fromDate . " 00:00:00";
                $fromTime = \DateTime::createFromFormat("d/m/Y H:i:s", $fromTime, new \DateTimeZone('Asia/Ho_Chi_Minh'))
                    ->setTimezone(new \DateTimeZone('UTC'))->format('Y-m-d H:i:s');
                $toDate = $itemTime->toTime;
                $toTime = $toDate . " 23:59:59";
                $toTime = \DateTime::createFromFormat("d/m/Y H:i:s", $toTime, new \DateTimeZone('Asia/Ho_Chi_Minh'))
                    ->setTimezone(new \DateTimeZone('UTC'))->format('Y-m-d H:i:s');
                $partIssues = DB::table('issues as i')
                    ->leftJoin('projects as p', 'p.id', '=', 'i.project_id')
                    ->leftJoin('users as u', 'u.id', '=', 'i.assigned_to_id')
                    ->where('i.updated_on', '>=', $fromTime)
                    ->where('i.updated_on', '<=', $toTime)
                    ->whereRaw('i.rgt - i.lft = 1')
                    ->get(['i.id', 'i.subject as name', 'i.status_id', 'i.priority_id', 'i.created_on', 'i.updated_on', 'p.name as project_name', 'i.project_id', 'u.firstname', 'u.lastname'])
                    ->toArray();
                $issues = array_merge($issues, $partIssues);
            }
        }

        $retVal = [];
        $defaultData = [
            'count' => 0,
            'ids' => [],
        ];
        $statuses = [
            5 => 'close',
            6 => 'reject',
        ];
        $priorities = [
            1 => 'low',
            2 => 'normal',
            3 => 'high',
            4 => 'urgent'
        ];
        foreach ($issues as $issue) {
            if (!isset($retVal[$issue->project_id])) {
                $retVal[$issue->project_id] = [
                    'info' => [
                        'id' => $issue->project_id,
                        'project_name' => $issue->project_name
                    ],
                    'issues' => [
                        'urgent' => $defaultData,
                        'high' => $defaultData,
                        'normal' => $defaultData,
                        'low' => $defaultData,
                        'reject' => $defaultData,
                        'close' => $defaultData,
                        'reopen' => $defaultData,
                        'total' => $defaultData,
                    ]
                ];
            }
            if (array_key_exists($issue->status_id, $statuses)) {
                $status = $statuses[$issue->status_id];
                $retVal = $this->handleIssue($retVal, $issue, $status);
            }
            if (array_key_exists($issue->priority_id, $priorities)) {
                $priority = $priorities[$issue->priority_id];
                $retVal = $this->handleIssue($retVal, $issue, $priority);
            }
            $retVal = $this->handleIssue($retVal, $issue, 'total');
            if ($this->checkReopen($issue->id)) {
                $retVal = $this->handleIssue($retVal, $issue, 'reopen');
            }
        }
        return response()->json([
            'status' => 'successful',
            'data' => array_values($retVal)
        ]);
    }

    private function handleIssue ($retVal, $issue, $key) {
        $retVal[$issue->project_id]['issues'][$key]['count'] ++;
        $retVal[$issue->project_id]['issues'][$key]['ids'][] = [
            'id' => $issue->id,
            'name' => $issue->name,
            'staff' => $issue->firstname . ' ' . $issue->lastname
        ];
        return $retVal;
    }

    private function checkReopen ($issueId) {
        $retVal = false;
        $journalIds = DB::table('journals')
            ->where('journalized_id', '=', $issueId)
            ->pluck('id')->all();
        if ($journalIds) {
            $journalDetail = DB::table('journal_details')
                ->whereIn('journal_id', $journalIds)
                ->where('property', 'attr')
                ->where('prop_key', 'status_id')
                ->where('value', 7)
                ->exists();
            if ($journalDetail) {
                $retVal = true;
            }
        }
        return $retVal;
    }

}
